var gulp = require('gulp');
// Requires the gulp-sass plugin
var sass = require('gulp-sass');

gulp.task('sass', function(){
    return gulp.src('MyProject/SCSS/**/*.scss')
      .pipe(sass()) // Using gulp-sass
      .pipe(gulp.dest('MyProject/CSS'))
      .pipe(browserSync.reload({
        stream: true}))
  });

  gulp.task('watch', function(){
    gulp.watch('MyProject/SCSS/**/*.scss', ['sass']);
})
var browserSync = require('browser-sync').create();
gulp.task('browserSync', function() {
    browserSync.init({
      server: {
        baseDir: 'osf/MyProject'
      },
    })
  })
   